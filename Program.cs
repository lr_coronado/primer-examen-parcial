﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimerParcial
{
    class Program
    {

        // Luis Rafael Coronado - 2020-10295

        static void Main(string[] args)
        {

            const string Banco = "FDP INVERSMENTS";
            const int BancoMax = 20000;
            const int OtroBancoMax = 10000;
            int Billete1000 = 18, Billete500 = 19, Billete200 = 23, Billete100 = 50, cantidadDeDinero = 0;
            cantidadDeDinero = (Billete1000*1000) + (Billete500*500) + (Billete200*200) + (Billete100*100);
            bool tieneDinero = true;

            do 
            {
                Console.WriteLine($"Que Banco Quiere usar? Introduce 1 para {Banco} - 2 para otro banco. ");
                Console.Write("-");
                int opcion = int.Parse(Console.ReadLine());

                if (cantidadDeDinero > 0)
                {

                    if (opcion == 1)
                    {

                        Console.Write("Cuanto quiere retirar? ");
                        int monto = int.Parse(Console.ReadLine());

                        if (monto <= BancoMax)
                        {
                            if (monto < cantidadDeDinero)
                            {
                                while (monto >= 1000)
                                {
                                    Console.WriteLine("Salida de Billete de {0:C}", 1000);
                                    Billete1000--;
                                    cantidadDeDinero -= 1000;
                                    monto -= 1000;
                                }

                                while (monto >= 500 && monto < 1000)
                                {
                                    Console.WriteLine("Salida de Billete de {0:C}", 500);
                                    Billete500--;
                                    cantidadDeDinero -= 500;
                                    monto -= 500;
                                }

                                while (monto >= 200 && monto < 500)
                                {
                                    Console.WriteLine("Salida de Billete de {0:C}", 200);
                                    Billete200--;
                                    cantidadDeDinero -= 200;
                                    monto -= 200;
                                }

                                while (monto >= 100 && monto < 200)
                                {
                                    Console.WriteLine("Salida de Billete de {0:C}", 100);
                                    Billete200--;
                                    cantidadDeDinero -= 100;
                                    monto -= 100;

                                }

                                Console.WriteLine("Cantidad Total en el cajero: {0:C}", cantidadDeDinero);
                                Console.WriteLine("Dinero por entregar: {0:C} \n", monto);
                            }
                        }
                        else
                        {
                            Console.WriteLine("No se puede retirar más de {0:C}\n", BancoMax);
                        }

                    }
                    else if (opcion == 2)
                    {

                        Console.Write($"Cuanto quiere retirar? (Maximo {10000:C})");
                        int monto = int.Parse(Console.ReadLine());

                        if (monto <= OtroBancoMax)
                        {
                            if (monto < cantidadDeDinero)
                            {
                                while (monto >= 1000)
                                {
                                    Console.WriteLine("Salida de Billete de {0:C}", 1000);
                                    Billete1000--;
                                    cantidadDeDinero -= 1000;
                                    monto -= 1000;
                                }

                                while (monto >= 500 && monto < 1000)
                                {
                                    Console.WriteLine("Salida de Billete de {0:C}", 500);
                                    Billete500--;
                                    cantidadDeDinero -= 500;
                                    monto -= 500;
                                }

                                while (monto >= 200 && monto < 500)
                                {
                                    Console.WriteLine("Salida de Billete de {0:C}", 200);
                                    Billete200--;
                                    cantidadDeDinero -= 200;
                                    monto -= 200;
                                }

                                while (monto >= 100 && monto < 200)
                                {
                                    Console.WriteLine("Salida de Billete de {0:C}", 100);
                                    Billete200--;
                                    cantidadDeDinero -= 100;
                                    monto -= 100;

                                }

                                Console.WriteLine("Cantidad Total en el cajero: {0:C}", cantidadDeDinero);
                                Console.WriteLine("Dinero por entregar: {0:C} \n", monto);
                            }
                        }
                        else
                        {
                            Console.WriteLine("No se puede retirar más de {0:C}\n", OtroBancoMax);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Esa no es una opcion valida.");
                        break;
                    }
                }
                else 
                {
                    Console.WriteLine("No hay dinero suficiente en el cajero.");
                    tieneDinero = false;
                }
            }
            while (tieneDinero == true);


        }
    }
}
